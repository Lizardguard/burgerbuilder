import React from 'react';
import BurgerIngredient from './BurgerIngredient/BurgerIngredient';
import classes from './Burger.module.css';

const burger = (props) => {
    return (
        <div className={classes.Burger}>
            <BurgerIngredient ingType="bread-top"/>
            <BurgerIngredient ingType="cheese"/>
            <BurgerIngredient ingType="meat"/>
            <BurgerIngredient ingType="bread-bottom"/>
        </div>
    );
}

export default burger;