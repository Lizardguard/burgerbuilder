import React  from 'react';

import Auxil from '../../hoc/Auxil';
import styles from './Layout.module.css';

const layout = (props) => (
    <Auxil>
        <div> Toolbar, SideDrawer, Backdrop</div>
        <main  className={styles.Content}>
            {props.children}
        </main>
    </Auxil>
);

export default layout;