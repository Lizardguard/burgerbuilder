import React, {Component} from 'react';
import Auxil from '../../hoc/Auxil'
import Burger from '../../components/Burger/Burger'
class BurgerBuilder extends Component {
    state = {};
    render () {
        return (
        <Auxil>
            <Burger/>
            <div>
                Build Controls
            </div>
        </Auxil>
        );
    }
}
export default BurgerBuilder;